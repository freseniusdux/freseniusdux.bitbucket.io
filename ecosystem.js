/*
 * A D3 node-link graph with demo data
 */
var
svg = d3.select('svg'),
width = +svg.attr('width'),
height = +svg.attr('height'),
color = d3.scaleOrdinal(d3.schemeAccent),
simulation = d3.forceSimulation();

function positionLink(d) {
    var
    dx = d[2].x - d[0].x,
    dy = d[2].y - d[0].y,
    dr = Math.sqrt(dx * dx + dy * dy);
    return 'M ' + d[0].x + ', ' + d[0].y + ' A ' + dr * 2 + ', ' + dr * 2  + ' 0 0,1 ' + d[2].x + ', ' + d[2].y;
}

function positionNode(d) {
    return 'translate(' + d.x + ', ' + d.y + ')';
}

function dragstarted(d) {
    if (!d3.event.active) {
        simulation.alphaTarget(0.3).restart();
        d.fx = d.x, d.fy = d.y;
    }
}

function dragged(d) {
    d.fx = d3.event.x, d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) { simulation.alphaTarget(0); }
    d.fx = null, d.fy = null;
}

function positionTextX(d) {
    let postion = (d.size * 2) + 3;
    if (d.x > width / 2) {
        return postion;
    } else {
        return - postion;
    }
}

function positionTextY(d) {
    let postion = (d.size * 2);
    if (d.y > height / 2) {
        return postion + 8;
    } else {
        return - (postion);
    }
}

function anchorText(d) {
    if (d.x > width / 2) {
        return 'start';
    } else {
        return 'end';
    }
}
simulation.force('center', d3.forceCenter(width / 2, height / 2))
    .force('charge', d3.forceManyBody().strength(-2))
    .force('collide', d3.forceCollide((d) => (d.size * 6) + 30 ).strength(0.9))
    .force('link', d3.forceLink().id( (d) => d.id ));

d3.json('ecosystem.json').then(function(data) {

    var
    nodes = data.getITAssetsResult,
    nodeById = d3.map(nodes, (d) => d.ITASSET_ID ),
    bilinks = [],
    links = [];

    nodes.forEach(function(asset) {

        var targetList = asset.mall_id;
        if (targetList) {
            // var targetArray = targetList.split('^').map(function(t) {
            //     return parseInt(t, 10);
            // });
            var targetArray = targetList.split('^');

            for (var i = 0; i < targetArray.length; i ++) {
                var edge = {};
                var source = asset.ITASSET_ID;
                edge.source = source;
                edge.target = targetArray[i];
                edge.value = targetArray.length + 1;
                links.push(edge);
            }
        }
    });

    function ticked() {
        link.attr('d', positionLink);
        node.attr('transform', positionNode);
        text.attr('x', positionTextX)
            .attr('y', positionTextY)
            .attr('text-anchor', anchorText);
    }


    function slugify(string) {
        return string.replace(/\s+/g, '-').toLowerCase();
    }

    links.forEach( function(link) {
        var
        s = link.source = nodeById.get(link.source),
        t = link.target = nodeById.get(link.target),
        i = {}; // intermediate node
        nodes.push(i);
        links.push({source: s, target: i}, {source: i, target: t});
        bilinks.push([s, i, t]);
    });


    var link = svg.selectAll('.link')
        .data(bilinks)
        .enter().append('path')
            .attr('class', 'link')
            .attr('stroke', (d) => color(d[0].group) );

    var node = svg.selectAll('.node')
        .data( nodes.filter( (d) => d.ITASSET_ID ))
        .enter().append('g')
            .attr('class', 'node-group')
            .attr('id', (d) => slugify(d.ITASSET_ID) )
            .call( d3.drag()
                .on('start', dragstarted)
                .on('drag', dragged)
                .on('end', dragended));

    var circle = node.append('circle')
        .attr('class', 'node')
        .attr('r', (d) => d.size * 2 )
        .attr('fill', (d) => color( d.group ) );

    var text = node.append('text')
        .text( (d) => d.ITASSET_ID )
        .attr('fill', 'black')
        .attr('x', positionTextX)
        .attr('y', positionTextY)
        .attr('text-anchor', anchorText);

    simulation.nodes(nodes).on('tick', ticked);

    simulation.force('link').links(links);

});
