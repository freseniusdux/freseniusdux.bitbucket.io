# The Fresenius Ecosystem Project

Preview at: [ecosystem.freseniusdux.bitbucket.io](https://ecosystem.freseniusdux.bitbucket.io).
This preview is set to robots: noindex for privacy, but is accessible from anywhere.

### Goal: to make an interactive graph of the Fresenius Tech ecosystem.

Second round TODOS

 - make links directional with arrowheads
 - read data from IT Navigator endpoint
 - make force factors more useful

FEATURES TO ADD:

- filter to make nodes disappear
- Search
- add People to data
- make workflow to save data from excel

Data endpoints from [IT Navigator](http://content.intranet.fmcna.com/itnavigator/);

- [AllBusinessFunctions](http://edmdocsvcdv01/Mall/Mall.svc/json/AllBusinessFunctions)
- [AllStewards](http://edmdocsvcdv01/Mall/Mall.svc/json/AllStewards)
- [AllCompanies](http://edmdocsvcdv01/Mall/Mall.svc/json/AllCompanies)
- [AllProcesses](http://edmdocsvcdv01/Mall/Mall.svc/json/AllProcesses)
- [ITAssets](http://edmdocsvcdv01/Mall/Mall.svc/json/ITAssets)
