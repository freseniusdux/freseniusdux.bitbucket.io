d3.json('ITAssets.json').then(function(data) {
    var assets = data.getITAssetsResult;
    var edges = [];

    assets.forEach(function(asset) {

        var targetList = asset.mall_id;
        if (targetList) {
            var targetArray = targetList.split('^');
            for (var i = 0; i < targetArray.length; i ++) {
                var edge = {};
                var source = asset.ITASSET_ID;
                edge.source = source.toString();
                edge.target = targetArray[i];
                var number = Math.random() * 10;
                edge.value = Math.round(number);
                edges.push(edge);
            }
        }
    });
    console.log(edges);
});
